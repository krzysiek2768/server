# Tank Server

Instalacja:

```bash
pip install pipenv
pipenv install
```

Uruchamianie:

```bash
python3 run.py
```

Uruchamianie serwera testowego:

```bash
python3 run.py server --mock localhost:9090
```