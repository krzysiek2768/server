# To be executed with pyinvoke, as in:
#
#    $ invoke TASK
#
#  where TASK is name of the task to execute (tasks are functions annotated with @task in this file).
#  For example:
#
#    $ invoke make-grpc
#
from invoke import task


@task
def make_grpc(c):
    from grpc_tools import protoc

    protoc.main(
        (
            '',
            '-I.',
            '--python_out=.',
            '--grpc_python_out=.',
            'tank/tank.proto',
        )
    )