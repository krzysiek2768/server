#!/usr/bin/env python3
import time
import grpc
from concurrent import futures
import logging
from argparse import ArgumentParser
from tank.util.pca9685 import PCA9685
from tank.server import TurretServicer, TankServicer
from tank.turret import TankTurret, MockTankTurret


def pulse(args):
    pwm = PCA9685(args.address)
    pwm.set_pwm_freq(args.freq)
    pwm.set_servo_pulse(args.channel, args.value)
    time.sleep(0.02)


def write(args):
    pwm = PCA9685(args.address)
    pwm.set_pwm_freq(args.freq)
    pwm.write(args.addr, args.value)
    time.sleep(0.02)


def server(args):
    if args.mock:
        turret = MockTankTurret(axes=[1, 2])
    else:
        turret = TankTurret(axes=[1, 2])

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=4))
    tank_servicer = TankServicer()
    turret_servicer = TurretServicer(turret)

    tank_servicer.add_to_server(server)
    turret_servicer.add_to_server(server)

    server.add_insecure_port(args.bind)
    server.start()
    print(f'Starting gRPC server at {args.bind}')

    try:
        while True:
            time.sleep(60 * 60)
    except KeyboardInterrupt:
        print('Stopping server')
        server.stop(0)
        exit(0)


parser = ArgumentParser()
# parser.add_argument('-c', '--config', type=str, default='config.toml')
parser.add_argument('-a', '--address', type=int, default=0x40)
parser.add_argument('-c', '--channel', type=int, default=1)
parser.add_argument('-f', '--freq', type=int, default=50)
parser.add_argument('--debug', action='store_const', const=True, default=False)

subparsers = parser.add_subparsers()

pulse_parser = subparsers.add_parser('pulse')
pulse_parser.add_argument('value', type=int, default=0)
pulse_parser.set_defaults(handler=pulse)

write_parser = subparsers.add_parser('write')
write_parser.add_argument('addr', type=int, default=0)
write_parser.add_argument('value', type=int, default=0)
write_parser.set_defaults(handler=write)

server_parser = subparsers.add_parser('server')
server_parser.add_argument('-m', '--mock', action='store_const', const=True, default=False)
server_parser.add_argument('bind', type=str, default=0)
server_parser.set_defaults(handler=server)

args = parser.parse_args()

logging.basicConfig(
    format='%(asctime)-15s %(clientip)s %(user)-8s %(message)s',
    level=logging.DEBUG if args.debug else logging.INFO
)

if hasattr(args, 'handler'):
    args.handler(args)
else:
    parser.print_help()

