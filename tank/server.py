import tank.tank_pb2_grpc as tank_grpc
import tank.tank_pb2 as tank_pb2


class TankServicer(tank_grpc.TankServicer):
    def move(self, request_iterator, context):
        for request in request_iterator:
            pass

        return tank_pb2.NoneResponse()

    def add_to_server(self, server):
        tank_grpc.add_TankServicer_to_server(self, server)


class TurretServicer(tank_grpc.TurretServicer):
    def __init__(self, arm):
        self.arm = arm

    def rotate(self, request_iterator, context):
        for request in request_iterator:
            pass

        return tank_pb2.NoneResponse()

    def add_to_server(self, server):
        tank_grpc.add_TurretServicer_to_server(self, server)
